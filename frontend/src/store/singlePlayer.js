import OperationClass from '@/services/operation'

let Operation = new OperationClass()

const state = {
  operation: null,
  corrects: 0,
  operationsLog: []
}

const mutations = {
  ADD_OPERATION_LOG (state, operationLog) {
    state.operationsLog = [
      operationLog,
      ...state.operationsLog
    ]
  },

  SET_OPERATION (state, operation) {
    state.operation = operation
  },

  INCREMENT_CORRECT (state) {
    state.corrects++
  }
}

const actions = {
  ADD_OPERATION_LOG ({ commit, rootGetters, rootState }, log) {
    commit('ADD_OPERATION_LOG', log)

    let createdAt = new Date()
    let userUuid = rootGetters['app/uuid']

    rootState.db.collection('singlePlayerData').add({
      createdAt,
      userUuid,
      ...log
    })
  },

  MAKE_OPERATION ({ commit }) {
    let newOperation = Operation.get()

    commit('SET_OPERATION', newOperation)
  }
}

const getters = {
  operationsLog (state) {
    return state.operationsLog.slice(0, 5)
  },

  currentOperation (state) {
    return state.operation
  },

  corrects (state) {
    return state.corrects || 0
  }
}

export default {namespaced: true, state, mutations, actions, getters}
