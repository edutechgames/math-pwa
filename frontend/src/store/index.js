import Vue from 'vue'
import Vuex from 'vuex'
import Firebase from '../services/firebase'
import 'babel-polyfill'

import app from './app'
import rooms from './rooms'
import singlePlayer from './singlePlayer'
import userData from './userData'

Vue.use(Vuex)

const db = Firebase.firestore()

db.settings({timestampsInSnapshots: true})

const state = {
  db: db
}

const store = new Vuex.Store({
  state,
  modules: {
    app,
    rooms,
    singlePlayer,
    userData
  }
})

store.subscribe((mutation, state) => {
  // Clear DB store instance causing circular structure reference
  let replaceState = Object.assign({}, state, {db: null})
  localStorage.setItem('store', JSON.stringify(replaceState))
})

export default store
