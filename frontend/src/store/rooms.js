const state = {
  rooms: [],
  roomsIds: []
}

const mutations = {
  SET_ROOMS (state, { room }) {
    const data = room.data()

    state.rooms = {
      ...state.rooms,
      [room.id]: data
    }
  },

  ADD_ROOM (state, room) {
    const data = room.data()

    if (!(data.id in state.rooms)) {
      state.rooms = {
        ...state.rooms,
        [room.id]: room.data()
      }
    }

    state.roomsIds = []
    state.roomsIds.push(room.id)
  },

  CHECK_ROOMS (state) {
    state.rooms.forEach(room => {
      if (!(room in state.roomsIds)) {
        delete state.rooms[room]
      }
    })
  }
}

const actions = {
  async get ({ commit, rootState }) {
    let roomsRef = rootState.db.collection('rooms').orderBy('createdAt')
    let rooms = await roomsRef.get()

    rooms.forEach(room => commit('SET_ROOMS', { room }))
  }
}

const getters = {
  listRooms (state) {
    return state.rooms
  }
}

export default {namespaced: true, state, mutations, actions, getters}
