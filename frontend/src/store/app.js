import firebase from 'firebase/app'
import 'firebase/auth'
import store from './index'
import uuid from 'uuid/v4'

const APP_STORE_KEY = 'store'
const UUID_KEY = 'user__uuid'

firebase.auth().onAuthStateChanged((user) => {
  if (!user) return
  store.dispatch('app/SET_USER_CREDENTIALS', user)
})

const state = {
  uuid: null,
  loginInProgress: false,
  userCredentials: null
}

const mutations = {
  SET_USER_UUID (state, uuid) {
    state.uuid = uuid

    localStorage.setItem(UUID_KEY, uuid)
  },

  SET_CREDENTIALS (state, credentials = null) {
    state.userCredentials = credentials
    state.loginInProgress = false
  },

  SET_LOGIN_PROCESS (state, status) {
    state.loginInProgress = status
  }
}

const actions = {
  INITIALIZE_STORE ({ rootState, commit }) {
    if (localStorage.getItem(APP_STORE_KEY)) {
      let replacementState = JSON.parse(localStorage.getItem(APP_STORE_KEY))
      delete replacementState.db

      this.replaceState(
        Object.assign(rootState, replacementState)
      )
    }

    if (!rootState.app.uuid) {
      let userUuid = uuid()

      commit('SET_USER_UUID', userUuid)
    }
  },

  CHECK_LOGIN ({ commit, state }) {
    if (state.loginInProgress && !state.userCredentials) {
      commit('SET_LOGIN_PROCESS', false)
    }
  },

  LOGIN_GOOGLE ({ getters, commit }) {
    /**
     * Validate if user is logged
     */
    if (getters['userCredentials']) {
      return
    }

    commit('SET_LOGIN_PROCESS', true)

    var provider = new firebase.auth.GoogleAuthProvider()
    firebase.auth().signInWithRedirect(provider)
  },

  LOGOUT_GOOGLE ({ commit }) {
    commit('SET_LOGIN_PROCESS', true)

    firebase.auth().signOut().then(() => {
      commit('SET_CREDENTIALS', null)
    })
  },

  async SET_USER_CREDENTIALS ({ commit, state, rootState, rootGetters }, credentials = null) {
    if (!credentials) return

    let data = credentials.providerData[0]

    let userDataDoc = rootState.db.collection('userData').doc(data.uid)
    let userData = await userDataDoc.get()

    if (!userData.exists) {
      data.uuids = [state.uuid] // Set uuid
      userDataDoc.set(data)
    } else {
      data = userData.data()

      if (data.uuids.indexOf(state.uuid) === -1) {
        data.uuids.push(state.uuid)
      }

      userDataDoc.update(data)
    }

    commit('SET_CREDENTIALS', data)
  }
}

const getters = {
  uuid (state) {
    if (state.userCredentials && state.userCredentials.uid) {
      return state.userCredentials.uid
    }

    return state.uuid
  },

  userCredentials (state) {
    return state.userCredentials
  },

  loginInProgress (state) {
    return state.loginInProgress
  }
}

export default {namespaced: true, state, getters, actions, mutations}
