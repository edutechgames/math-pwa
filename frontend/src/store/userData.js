const state = {
  history: [],
  historyLength: 0,
  showNextButton: false,
  prevDate: []
}

const mutations = {
  UPDATE_HISTORY (state, history) {
    state.history = history
  },

  SET_HISTORY_LENGTH (state, length) {
    state.historyLength = length
  },

  SET_NEXT_BUTTON_STATE (state, status = false) {
    state.showNextButton = status
  }
}

const actions = {
  async FETCH_HISTORY ({ state, commit, rootState, rootGetters }, params = {}) {
    if (!params.limit) params.limit = 5

    let userUuids = rootGetters['app/userCredentials'].uuids.concat(rootGetters['app/uuid'])

    let historyRef = rootState.db.collection('singlePlayerData')
      .orderBy('createdAt', 'desc')

    userUuids.map(uuid => {
      historyRef.where('userUuid', '==', uuid)
    })

    if ('next' in params) {
      let last = state.history[state.history.length - 1]
      let first = state.history[0]

      state.prevDate.push(first.createdAt.toDate())
      historyRef = historyRef.where('createdAt', '<', last.createdAt.toDate())
    }

    if ('prev' in params) {
      let first = state.history[0]

      historyRef = historyRef.where('createdAt', '>', first.createdAt.toDate())
      historyRef = historyRef.where('createdAt', '<=', state.prevDate.pop())
    }

    historyRef.limit(params.limit).get().then(response => {
      let data = []

      if (response.size === 0) {
        commit('SET_NEXT_BUTTON_STATE', false)
        return
      }

      if (response.size < 5) {
        commit('SET_NEXT_BUTTON_STATE', false)
      } else {
        commit('SET_NEXT_BUTTON_STATE', true)
      }

      response.forEach(i => {
        data.push(i.data())
      })

      commit('SET_HISTORY_LENGTH', response.size)
      commit('UPDATE_HISTORY', data)
    })
  }
}

const getters = {
  history ({ history }) {
    return history
  },

  historyLength ({ historyLength }) {
    return historyLength
  },

  showNextButton ({ showNextButton }) {
    return showNextButton
  }
}

export default {namespaced: true, state, mutations, actions, getters}
