import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import SinglePlayer from '@/pages/SinglePlayer'
import Multiplayer from '@/pages/Multiplayer'
import History from '@/pages/History'
import VueAnalytics from 'vue-analytics'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/single-player',
      name: 'single-player',
      component: SinglePlayer
    },
    {
      path: '/multiplayer',
      name: 'multiplayer',
      component: Multiplayer
    },
    {
      path: '/history',
      name: 'history',
      component: History
    },
    {
      path: '*',
      name: 'catch-all',
      component: Home
    }
  ]
})

Vue.use(VueAnalytics, {
  id: process.env.GA_ID,
  router
})

export default router
