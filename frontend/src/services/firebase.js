import config from '../config/firebase'
import Firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const firebase = Firebase.initializeApp(config)

export default firebase
