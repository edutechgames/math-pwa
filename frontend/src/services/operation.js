import shuffle from '../utils/shuffle'

const DEFAULT_OPERATORS = ['-', '+', '*']

const DEFAULT_MIN_VALUE = 1

const DEFAULT_MAX_VALUE = 100

const MakeOperation = (term1, term2, op) => {
  if (!term1 || !term2 || !op) {
    throw new Error('Invalid arguments.')
  }

  return `${term1} ${op} ${term2}`
}

const Random = (operandMin, operandMax) => {
  return Math.floor(Math.random() * (operandMax - operandMin + 1) + operandMin)
}

const RandomOperator = (operators) => {
  if (!operators || !(operators instanceof Array)) {
    throw new Error('Operators is not an array.')
  }

  return operators[Random(0, operators.length - 1)]
}

const Eval = (operation) => {
  /* eslint no-eval: 0 */
  return eval(operation)
}

export default class Operation {
  constructor (operandMin = DEFAULT_MIN_VALUE, operandMax = DEFAULT_MAX_VALUE, operators = DEFAULT_OPERATORS) {
    this.operandMin = operandMin
    this.operandMax = operandMax
    this.operators = operators
  }

  get () {
    let term1 = Random(this.operandMin, this.operandMax)
    let term2 = Random(this.operandMin, this.operandMax)
    let operator = RandomOperator(this.operators)

    let operation = MakeOperation(term1, term2, operator)
    let correctAnswer = Eval(operation)
    let alternatives = [
      correctAnswer
    ]

    for (let i = 0; i < 3; i++) {
      let altTerm1 = Random(this.operandMin, this.operandMax)
      let altTerm2 = Random(this.operandMin, this.operandMax)

      alternatives.push(Eval(MakeOperation(altTerm1, altTerm2, operator)))
    }

    let createdAt = new Date()

    return {
      correctAnswer,
      operation,
      alternatives: shuffle(alternatives),
      term1,
      term2,
      operator,
      createdAt
    }
  }

  validateAnswer (operation, answer) {
    return Eval(operation) === parseInt(answer)
  }
}
