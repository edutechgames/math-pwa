// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

import 'materialize-css/dist/js/materialize'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',

  beforeCreate () {
    this.$store.dispatch('app/INITIALIZE_STORE')
  },

  router,
  store,
  template: '<App/>',
  components: { App }
})
