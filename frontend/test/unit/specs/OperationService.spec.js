import chai from 'chai'
import Operation from '../../../src/services/operation'

let expect = require('chai').expect
let assert = require('chai').assert

chai.should()

const DEFAULT_OPERATORS = ['-', '+', '*']

// let Operation = require(path.join(__dirname, '..', 'services', 'operation'))

describe('Operation', () => {
  describe('#constructor()', () => {
    it('Allow no argument', () => {
      expect(() => {
        // eslint-disable-next-line
        new Operation();
      }).to.not.throw(Error)
    })
  })

  describe('Range 1-100', () => {
    let operation
    let operandMin = 1
    let operandMax = 100

    beforeEach(() => {
      operation = new Operation()
    })

    it('Returns valid first term', () => {
      assert(true, (operation.term1 >= operandMin && operation.term1 <= operandMax))
    })

    it('Returns valid second term', () => {
      assert(true, (operation.term1 >= operandMin && operation.term1 <= operandMax))
    })

    it('Returns valid default operators', () => {
      assert(true, operation.operator in DEFAULT_OPERATORS)
    })
  })

  describe('Range 100-1000', () => {
    let operation
    let operandMin = 100
    let operandMax = 1000

    beforeEach(() => {
      operation = new Operation(operandMin, operandMax)
    })

    it('Returns valid first term', () => {
      assert(true, (operation.term1 >= operandMin && operation.term1 <= operandMax))
    })

    it('Returns valid second term', () => {
      assert(true, (operation.term1 >= operandMin && operation.term1 <= operandMax))
    })

    it('Returns valid default operators', () => {
      assert(true, operation.operator in DEFAULT_OPERATORS)
    })
  })

  describe('Operators', () => {
    let operators = ['-', '+']

    it('Should return valid operator', () => {
      let operation = new Operation(1, 100, operators)

      assert(true, operation.operators in operators)
    })

    it('Should not return invalid operator', () => {
      let operation = new Operation(1, 100, ['*'])

      assert(true, !(operation.operator in operators))
    })
  })
})
